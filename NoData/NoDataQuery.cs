using System.Linq.Expressions;
using NoData.QueryParser.ParsingTools;
using NoData.GraphImplementations.Schema;
using Microsoft.AspNetCore.Http;
using NoData.Utility;
using GraphLibrary;
using NoData.GraphImplementations.Queryable;
using System.Text;

namespace NoData
{
    public class NoDataQuery<TDto> : INoDataQuery<TDto>
        where TDto : class, new()
    {
        //private readonly HttpContextAccessor httpContextAccessor;

        // public FilterSecurityTypes FilterSecurity { get; } = FilterSecurityTypes.AllowOnlyVisibleValues;

        // Properties that represent the object model. Ie setup operations
        public Parameters? Parameters { get; }
        protected IQueryable<TDto> Source { get; }

        // Properties that are a result of parsing.
        internal IEnumerable<ITuple<PathToProperty, SortDirection>> OrderByPath { get; }
        private ParameterExpression DtoExpression { get; }
        private Expression? FilterExpression { get; }
        private Expression? SelectExpandExpression { get; }
        private QueryTree SelectionTree { get; }
		public Action<int> ApplyCount { get; set; } = (_) => {};

        private IClassCache Cache { get; }

        public NoDataQuery(
            IQueryable<TDto> source,
            Parameters parameters,
            IClassCache cache,
            IEnumerable<ITuple<PathToProperty, SortDirection>> orderBy,
            Expression? selectExpandExpression,
            Expression? filterExpression,
            ParameterExpression dtoParameterExpression,
            QueryTree selectionTree
			)
        {
            Source = source;
            Parameters = parameters;

            SelectionTree = selectionTree;
            SelectExpandExpression = selectExpandExpression;
            DtoExpression = dtoParameterExpression;
            FilterExpression = filterExpression;
            OrderByPath = orderBy;

            Cache = cache;
        }


        /// <summary>
        /// Applies the top term.
        /// </summary>
        private IQueryable<TDto> ApplyTop(IQueryable<TDto> query)
        {
            if (Parameters?.Top.HasValue ?? false)
                query = query.Take(Parameters.Top.Value);
            return query;
        }

        /// <summary>
        /// Applies the skip term
        /// </summary>
        private IQueryable<TDto> ApplySkip(IQueryable<TDto> query)
        {
            if (Parameters?.Skip.HasValue ?? false)
                query = query.Skip(Parameters.Skip.Value);
            return query;
        }

        /// <summary>
        /// Applies the filter query string the queryable.
        /// </summary>
        private IQueryable<TDto> ApplyFilter(IQueryable<TDto> query)
        {
            if (FilterExpression != null || !string.IsNullOrWhiteSpace(Parameters?.Filter))
                query = Utility.ExpressionBuilder.ApplyFilter(query, DtoExpression, FilterExpression!);
            return query;
        }

        /// <summary>
        /// Selects a subset of properties
        /// </summary>
        /// <remarks>May require that Apply Expand is called first.</remarks>
        private IQueryable<TDto> ApplySelect(IQueryable<TDto> query)
        {
			if(SelectExpandExpression is not null)
				query = Utility.ExpressionBuilder.ApplySelectExpand(DtoExpression, SelectExpandExpression, query);
            return query;
        }

        /// <summary>
        /// Selects a subset of properties
        /// </summary>
        /// <remarks>May require that Apply Expand is called first.</remarks>
        private IQueryable<TDto> ApplyOrderBy(IQueryable<TDto> query)
        {
            if (!string.IsNullOrEmpty(Parameters?.OrderBy))
                query = OrderByClauseParser<TDto>.OrderBy(query, OrderByPath, DtoExpression);
            return query;
        }

        /// <summary>
        /// Parses and then applies the filtering to the queryable, without enumerating it.
        /// </summary>
        private IQueryable<TDto> Apply()
        {
            var query = this.Source;
			query = ApplyFilter(query);
			query = ApplySelect(query);
			query = ApplyOrderBy(query);
			if(Parameters?.Count == true)
				ApplyCount(query.Count());
			query = ApplySkip(query);
			return ApplyTop(query);
        }

        public IQueryable<TDto> BuildQueryable() => Apply();

        /// <summary>
        /// Returns the json result of the queryable with all query commands parsed and applied.
        /// </summary>
        public string AsJson()
        {
			var sb = new StringBuilder();
			foreach(var data in this.Stream().ToEnumerable())
			{
				sb.Append(data);
			}
			return sb.ToString();
        }

        public async Task StreamResponse(HttpResponse response)
        {
            response.StatusCode = StatusCodes.Status200OK;
            response.ContentType = "application/json";
			await foreach(var data in this.Stream())
				await response.Body.WriteAsync(Encoding.UTF8.GetBytes(data));
			await response.Body.FlushAsync();
			//response.Body.Close();
        }

        public async Task StreamResponse(Stream response)
        {
			await foreach(var data in this.Stream())
				await response.WriteAsync(System.Text.Encoding.UTF8.GetBytes(data));
		}

        public async IAsyncEnumerable<string> Stream()
        {
			// could have a buffer here to reduce the number of "yield returns".
			var first = true;
			await foreach(var item in Apply().ToAsyncEnumerable())
			{
				if(first)
					yield return "[" + this.SelectionTree.AsJson(item);
				else
					yield return "," + this.SelectionTree.AsJson(item);
				first = false;
			}
			yield return "]";
        }
    }
}
