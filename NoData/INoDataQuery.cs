using Microsoft.AspNetCore.Http;

namespace NoData
{

    /// <summary>
    /// This interface is designed to have just enough information to apply the expression tree to the query, and return the result query in the correct format.
    /// </summary>
    public interface INoDataQuery<out TDto>
    {
        IQueryable<TDto> BuildQueryable();
		Action<int> ApplyCount { get; set; }

        string AsJson();
        Task StreamResponse(HttpResponse response);
        Task StreamResponse(Stream responseBody);
        IAsyncEnumerable<string> Stream();
    }
}
