﻿namespace SampleEFCoreApi;

public static class Program
{
	public static void Main(string[] args)
	{
		BuildWebHost(args).Build().Run();
	}

	public static IHostBuilder BuildWebHost(string[] args)
	{
		return Host.CreateDefaultBuilder(args)
			.ConfigureWebHostDefaults(webBuilder => {
				webBuilder.ConfigureLogging(x => {
					x.ClearProviders();
					x.AddConsole();
					x.AddDebug();
				 });
				webBuilder.UseUrls("http://localhost:3000");
				webBuilder.UseStartup<Startup>();
			})
			;
	}
}
