﻿using Microsoft.EntityFrameworkCore;
using NoData;

namespace SampleEFCoreApi;

public class Startup
{
	private static string InstanceName = Guid.NewGuid().ToString();

	public Startup(IConfiguration configuration)
	{
		Configuration = configuration;
	}

	public IConfiguration Configuration { get; }

	public void ConfigureServices(IServiceCollection services)
	{
		services.AddOptions();
		services.AddControllers();
		services.AddNoData();
		services.AddDbContext<DataContext>(x => x.UseInMemoryDatabase(Configuration.GetValue("test-context", InstanceName)));
			// .UseSqlServer(@"Server=localhost;Database=MyDb;User=sa;Password=YourStrong!Passw0rd;")
			// .UseLoggerFactory(MyLoggerFactory)
		AutoMapperConfig.DependencyInjection(services);
	}

	// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
	public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
	{
		if (env.IsDevelopment())
		{
			app.UseDeveloperExceptionPage();
		}
		app.UseRouting();
		app.UseEndpoints(x => x.MapControllers());
	}
}
