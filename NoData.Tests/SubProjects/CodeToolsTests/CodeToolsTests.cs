using Xunit;

// Thanks to https://blogs.msdn.microsoft.com/kevinpilchbisson/2007/11/20/enforcing-immutability-in-code/
namespace NoData.Tests.SubProjects.CodeToolsTests
{
    public class CodeToolsTests
    {
        private Immutability.Test immutability = new Immutability.Test(new[] {
                    typeof(Settings),
                });


        [Fact]
        public void EnsureStructsAreImmutableTest()
        {
            immutability.EnsureStructsAreImmutableTest();
        }

        [Fact]
        public void EnsureImmutableTypeFieldsAreMarkedImmutableTest()
        {
            immutability.EnsureImmutableTypeFieldsAreMarkedImmutableTest();
        }
    }
}
