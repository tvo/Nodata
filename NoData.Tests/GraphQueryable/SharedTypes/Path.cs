namespace NoData.Tests.GraphQueryable.SharedTypes
{
    public class Path : GraphLibrary.Path<Edge, Vertex, string, string>
    {
        public Path(IEnumerable<Edge> edges) : base(edges)
        {
        }
    }
}
