namespace NoData.Tests.GraphQueryable.SharedTypes
{
    public class Vertex : GraphLibrary.Vertex<string>
    {
        public Vertex(string value) : base(value)
        {
        }
    }
}
