namespace NoData.Tests.GraphQueryable.SharedTypes
{
    public class Edge : GraphLibrary.Edge<string, Vertex, string>
    {
        public Edge(Vertex from, Vertex to, string value = null) : base(from, to, value)
        {
        }
    }
}
